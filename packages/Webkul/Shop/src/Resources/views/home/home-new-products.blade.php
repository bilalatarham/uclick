<!-- New arrivals -->
    <section class="related-products sale-products arrivals-product product-wrap section-padding new-arrival-d" style="display:none">
        <div class="container">
            <h1 class="sec-title-wrap">new arrivals</h1>  
            <p class="text-detail" style="display: block;"><span>Every solitaire tells a story of festive sparkle. </span>Get timeless designs with unmatched craftsmanship and high-quality gemstones.</p>
            <div id="new-products-section" style="text-align:center;"></div>
        </div>
    </section>
<!-- End -->