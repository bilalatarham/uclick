@if ($logo = core()->getCurrentChannel()->logo_url)
    <img src="{{ $logo }}" alt="{{ config('app.name') }}" style="width: 110px;"/>
@else
    <img src="{{ bagisto_asset('images/UClick.png') }}">
@endif